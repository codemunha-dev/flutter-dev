import 'package:flutter/material.dart';

class IndexScreen extends StatefulWidget {
 

  _IndexScreen createState() => _IndexScreen();
}

class _IndexScreen extends State<IndexScreen> {

  TextStyle myStyle = TextStyle(fontSize: 30.0);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Card(
          child: Image( image: AssetImage('assets/images/banner_vert.jpeg')),
        ),
        SizedBox( height: 20.0,),
        Card(
          child: Image( image: AssetImage('assets/images/banner.png')),
        ),
        SizedBox( height: 20.0,),
         Card(
          child: Image( image: AssetImage('assets/images/pic2.jpg')),
        ),
        
      ],
    
    );
  }
}