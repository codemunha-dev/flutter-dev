import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  _Settings createState() => _Settings();
}

class _Settings extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Card(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Icon(Icons.settings),
                  ),
                  Expanded(
                    flex: 8,
                    child: Text(
                      'ตั้งค่าการใช้งาน',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'http://fiyazhasan.me/content/images/2017/06/default-avatar-png.png'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('ข้อมูลส่วนตัว'),
              subtitle: Text('จัดการข้อมูลส่วนตัว'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://img.lovepik.com/element/40079/1888.png_300.png'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('เปลี่ยนรหัสผ่าน'),
              subtitle: Text('เปลี่ยนแปลงรัสผ่าน'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://manysolutions.com/media/catalog/product/p/r/product.png'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('จัดการสินค้า'),
              subtitle: Text('จัดการข้อมูลสินค้า'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
             ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://icon-library.net/images/catalogue-icon/catalogue-icon-28.jpg'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('หมวดหมูสินค้า'),
              subtitle: Text('จัดการหมวดหมู่ข้อมูลสินค้า'),
              trailing: Icon(Icons.keyboard_arrow_right),
            )
          ],
        )),
        Card(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Icon(Icons.settings),
                  ),
                  Expanded(
                    flex: 8,
                    child: Text(
                      'ตั้งค่าการบัญเบื้องต้น',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://www.cocodimama.co.uk/wp-content/uploads/2017/08/deliver-order-now.png'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('ออร์เดอร์และการสั่งซื้อ'),
              subtitle: Text('จัดการข้อมูลการสั่งซื้อต่างๆ'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://i7.pngguru.com/preview/953/232/878/payment-gateway-computer-icons-e-commerce-payment-system-payment-thumbnail.jpg'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('การชำระเงินและค้างชำระ'),
              subtitle: Text('รายการชำระเงินต่างๆ'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://www.set.or.th/financialplanning/Image/lifeevent/detail_postretire/postretire-4_cover_1.png'),
                backgroundColor: Colors.amberAccent,
              ),
              title: Text('การเร่งรัดหนี้สิ้น'),
              subtitle: Text('จัดการหนี้สินและเร่งรัดหนี้สิน'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
        
          ],
        )),
      ],
    );
  }
}
