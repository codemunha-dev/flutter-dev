import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  _MainScreen createState() => _MainScreen();
}

class _MainScreen extends State<MainScreen> {
  // String
  String firstName = 'Thinny';
  String lastName = 'injitt';
  String description = '''
  sutin injitt
  flutter developer
  ''';

  // Integer
  int age = 30;
  double height = 220.34;

  // var
  var descript = 'variable by dynamic';

  // Map
  Map types = {'type': 'admin'};
  Map<String, int> types2 = {'age': 20};

  // List
  List sex = ['male', 'female'];
  List<int> ages = [10, 20];
  List<Map<String, int>> users = [
    {'weight': 80, 'height': 20}
  ];

  // function|method
  void showName(String name, String lastName) {
    print('Name: $name, Last: $lastName');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Flutter My Application")),
        body: Center(
          child: RaisedButton(
            onPressed: () {
              print('$firstName $lastName');
              double result = age + height;
              print('$result');

              showName('sutin', 'injitt');

              Person p = Person('flutter name');
              p.showName();
            },
            color: Colors.green,
            textColor: Colors.white,
            child: Text("Click Me"),
          ),
        ));
  }
}

class Person {
  String name;

  // contructor
  Person(this.name);

  // method
  showName() {
    print('Person: $name');
  }
}
