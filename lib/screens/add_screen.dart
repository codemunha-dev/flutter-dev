import 'package:flutter/material.dart';

class AddScreen extends StatefulWidget {

  var params;

  AddScreen(this.params);

  _AddScreen createState() => _AddScreen(params);
}

class _AddScreen extends State<AddScreen> {
  var _params;

  _AddScreen(this._params);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add new Pages"),
      ),
      body: ListView(
        children: <Widget>[
          Text("List #1"),
          Text("Parmas: $_params", style: TextStyle(fontSize: 20.0)),
          RaisedButton(
            color: Colors.indigo,
            onPressed: () => {
              Navigator.of(context).pop({ 'data': 'Back OK'}),
            },
            child: Text("กลับไป", style: TextStyle(color: Colors.white, fontSize: 20.0)),
          )
        ],
      ),
    );
  }
}