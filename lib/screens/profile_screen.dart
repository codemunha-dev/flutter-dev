import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  _UserProfile createState() => _UserProfile();
}

class _UserProfile extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 100.0,
              color: Colors.red,
            ),
            Container(
              height: 100.0,
              color: Colors.green,
            ),
            Container(
              height: 100.0,
              color: Colors.blue,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 100.0,
                    color: Colors.pink,
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 100.0,
                    color: Colors.orange,
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 200.0,
                    color: Colors.yellow,
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 200.0,
                    color: Colors.black,
                  ),
                )
              ],
            ),
             Row(
              children: <Widget>[
                Expanded(
                  child: Image( 
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/banner_vert.jpeg'))
                ),
                Expanded(
                  child:  Image( 
                     fit: BoxFit.cover,
                    image: AssetImage('assets/images/banner.png'))
                )
              ],
            ),
            
          ],
        ),
      ],
    );
  }
}
