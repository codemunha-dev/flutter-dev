import 'package:flutter/material.dart';

// Http Services
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:convert';

class UserScreen extends StatefulWidget {
  _UserScreen createState() => _UserScreen();
}

class _UserScreen extends State<UserScreen> {
  var users;
  bool isLoading = true;

// Async method for Futre only
  Future<Null> getUsers() async {
    final response = await http.get("https://randomuser.me/api/?results=10");
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      print(jsonResponse);

      setState(() {
        users = jsonResponse['results'];
        isLoading = false;
      });
    } else {
      print("Connection Fialed");
    }
  }

// Hook LifeCycile
  @override
  void initState() {
    super.initState();
    getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('จัดการข้อมูลผู้ใช้งาน'),
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : RefreshIndicator(
              onRefresh: getUsers,
              child: Card(
                child: ListView.builder(
                  itemBuilder: (context, int index) {
                    return Column(
                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(
                            backgroundImage: NetworkImage(users[index]['picture']['medium']),
                            backgroundColor: Colors.blueAccent,
                          ),
                          onTap: () {},
                          title: Text(
                            'Name: ${users[index]['name']['first']} - ${users[index]['name']['last']} ',
                            style: TextStyle(fontSize: 20.0),
                          ),
                          subtitle: Text(
                            'Sreet: ${users[index]['location']['street']}',
                            style: TextStyle(fontSize: 16.0),
                          ),
                          trailing: Icon(Icons.keyboard_arrow_right),
                        ),
                        Divider()
                      ],
                    );
                  },
                  itemCount: users != null ? users.length : 0,
                ),
              ),
            ),
    );
  }
}
