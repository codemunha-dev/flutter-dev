import 'package:demo/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LoginScreen extends StatefulWidget {
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();

  void doLogin() {
    print(_email.text);
    print(_password.text);

    if(_email.text == 'sutin17@hotmail.com'){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          color: Colors.greenAccent,
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 50.0),
              Image(
                  width: 200.0,
                  height: 200.0,
                  image: NetworkImage(
                      "https://img11.androidappsapk.co/300/6/7/7/com.iphone.lockscreen.ios.lock.screen.iphonex.png")),
              Padding(
                padding: const EdgeInsets.all(50.0),
                child: Form(
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        controller: _email,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            labelText: "Email",
                            prefixIcon: Icon(Icons.email),
                            filled: true,
                            fillColor: Colors.white),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        controller: _password,
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: "Password",
                            prefixIcon: Icon(Icons.vpn_key),
                            filled: true,
                            fillColor: Colors.white),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          MaterialButton(
                            color: Colors.red,
                            clipBehavior: Clip.antiAlias,
                            shape: StadiumBorder(),
                            child: Text("Login System"),
                            textColor: Colors.white,
                            onPressed: () => doLogin(),
                          ),
                          MaterialButton(
                            color: Colors.white,
                            clipBehavior: Clip.antiAlias,
                            shape: StadiumBorder(),
                            child: Text("Clear Data"),
                            onPressed: () {},
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    ));
  }
}
