import 'dart:io';

import 'package:flutter/material.dart';
import 'package:demo/screens/index_screen.dart';
import 'package:demo/screens/profile_screen.dart';
import 'package:demo/screens/setting_screen.dart';
import 'package:demo/screens/add_screen.dart';

class HomeScreen extends StatefulWidget {
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  int _currentIndex = 0;
  List pages = [IndexScreen(), UserProfile(), Settings()];

  @override
  Widget build(BuildContext context) {
    Widget appBar = AppBar(
      title: Text('หน้าโฮม'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.home),
          onPressed: () => {Navigator.of(context).pushNamed('/add')},
        ),
        IconButton(
          icon: Icon(Icons.verified_user),
          onPressed: () {},
        )
      ],
    );

    Widget floatingAction = FloatingActionButton(
      onPressed: () async {
        var resp = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AddScreen(DateTime.now().toString())));

        print(resp['data']);
      },
      child: Icon(Icons.add),
      backgroundColor: Colors.red,
    );

    Widget bottomNavBar = BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (int index) => {
              setState(() {
                _currentIndex = index;
              })
            },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home), title: Text('หน้าแรก')),
          BottomNavigationBarItem(
              icon: Icon(Icons.verified_user), title: Text('โปรไฟล์')),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), title: Text('ตั้งค่า')),
        ]);

    Widget drawer = Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://i.dlpng.com/static/png/4062147-avatar-user-computer-icons-software-developer-avatar-png-png-computer-user-900_540_preview.png"),
            ),
            accountEmail:
                Text("thinny@gmail.com", style: TextStyle(fontSize: 18.0)),
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(
                        "https://cdn.pixabay.com/photo/2015/12/01/08/44/banner-1071797_960_720.jpg"))),
            accountName: Text(
              "Thinny",
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
            ),
          ),
          ListTile(
              leading: Icon(Icons.assignment_turned_in),
              trailing: Icon(Icons.arrow_forward),
              title: Text('สินค้าทั้งหมด', style: TextStyle(fontSize: 20.0)),
              subtitle: Text("รายการสินค้าทั้งหมด"),
              onTap: () {}),
          ListTile(
              leading: Icon(Icons.beenhere),
              trailing: Icon(Icons.arrow_forward),
              title: Text('หมวดหมุ่สินค้า', style: TextStyle(fontSize: 20.0)),
              subtitle: Text("รายการหมวดหมุ่สินค้าทั้งหมด"),
              onTap: () {}),
          ListTile(
              leading: Icon(Icons.settings),
              trailing: Icon(Icons.arrow_forward),
              title: Text('ตั้งค่าระบบ', style: TextStyle(fontSize: 20.0)),
              subtitle: Text("การตั้งค่าในระบบ"),
              onTap: () {}),
          ListTile(
              leading: Icon(Icons.group_work),
              trailing: Icon(Icons.arrow_forward),
              title: Text('ผู้ใช้ทั้งหมด', style: TextStyle(fontSize: 20.0)),
              subtitle: Text("รายการผู้ใช้ทั้งหมด"),
              onTap: () {
                Navigator.of(context).pushNamed('/users');
              }),
          Divider(),
          ListTile(
              trailing: Icon(Icons.exit_to_app),
              title: Text('ออกจากแอพ', style: TextStyle(fontSize: 20.0)),
              subtitle: Text("การออกจากแอพ"),
              onTap: () {
                exit(0);
              }),
        ],
      ),
    );

    return Scaffold(
      appBar: appBar,
      drawer: drawer,
      body: pages[_currentIndex],
      floatingActionButton: floatingAction,
      bottomNavigationBar: bottomNavBar,
    );
  }
}
