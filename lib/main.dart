
import 'package:demo/screens/add_screen.dart';
import 'package:demo/screens/profile_screen.dart';
import 'package:demo/screens/setting_screen.dart';
import 'package:flutter/material.dart';
import 'package:demo/screens/users_screen.dart';
//import 'package:demo/screens/home_screen.dart';
import 'package:demo/screens/login_screen.dart';

void main () => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
   return MaterialApp(
     title: 'Flutter Demo App',
     home: LoginScreen(),// HomeScreen(),
     theme: ThemeData(
       scaffoldBackgroundColor: Colors.white60,
       primaryColor: Colors.pink,
       accentColor: Colors.amber,
       fontFamily: 'salmon'
     ),
     debugShowCheckedModeBanner: false,
     routes: <String, WidgetBuilder>{
       '/add': (BuildContext context) => AddScreen("Hello From Route"),
       '/profile' : (BuildContext context) => UserProfile(),
       '/setting' : (BuildContext context) => Settings(),
       '/users' : (BuildContext context) => UserScreen()
     },
   );
  }
}

