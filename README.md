# demo

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## Stage
 - StatelessWidget ==> never update data in State
 - StatefulWidget ==> has update data in state

## Functional
 - () {} ==> not callback function()
 - () => {} ==> arrow function has callback function

## logs
 - print('string text') ==> print text in log
 - print('$variable1 $varable2') ==> passed variable in log
 - print('${1+2}') ==> caculate or condition in log

## function | method
 - when has method in class use name method | this.name_method

## accessibility
 - private ==> use _property_name

## class 
 - contructor ==> Person(this.name) to assign data to variable

## assign global variable
 - setState  ==> apply global variable for StateFull only

## layout
 - use Expland Widget inner Container
=======
# screenshort

![alt text](screenshort/basic-card.png)
>>>>>>> 576172af03e8463ff6c9bb3ce0c2a43967ca4dcd
